﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class Context : DbContext
    {
        public Context(DbContextOptions options): base(options)
        { }

        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Transaction>().ToTable("transaction");
            builder.Entity<Transaction>().Property(x => x.Id).HasColumnName("id");
            builder.Entity<Transaction>().Property(x => x.TransactionId).HasColumnName("identificator");
            builder.Entity<Transaction>().Property(x => x.Amount).HasColumnName("amount");
            builder.Entity<Transaction>().Property(x => x.CurrencyCode).HasColumnName("currency_code");
            builder.Entity<Transaction>().Property(x => x.TransactionDate).HasColumnName("date");
            builder.Entity<Transaction>().Property(x => x.Status).HasColumnName("status");
            base.OnModelCreating(builder);
        }
    }
}

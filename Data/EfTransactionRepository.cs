﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Entities;
using Domain.Models;

namespace Data
    {
    public class EfTransactionRepository : EfRepository<Transaction>, ITransactionRepository
        {
        public EfTransactionRepository(Context context) : base(context)
            {
            }

        public List<TransactionViewModel> GetTransactionsByCurrency(string currency)
            {
            return Context.Transactions.Where(x => x.CurrencyCode == currency)
                .Select(x => new TransactionViewModel
                    {
                    id = x.TransactionId,
                    payment = x.Amount + " " + x.CurrencyCode,
                    Status = ((Status)x.Status).ToString()
                    }).ToList();
            }

        public List<TransactionViewModel> GetTransactionsByDateRange(DateTime start, DateTime end)
            {
            return Context.Transactions.Where(x => x.TransactionDate > start && x.TransactionDate <= end).Select(x => new TransactionViewModel
                {
                id = x.TransactionId,
                payment = x.Amount + " " + x.CurrencyCode,
                Status = ((Status)x.Status).ToString()
                }).ToList();
            }

        public List<TransactionViewModel> GetTransactionsByStatus(short status)
            {
            return Context.Transactions.Where(x => x.Status == status).Select(x => new TransactionViewModel
                {
                id = x.TransactionId,
                payment = x.Amount + " " + x.CurrencyCode,
                Status = ((Status)x.Status).ToString()
                }).ToList();
            }

        public bool AddTransactionsList(List<Transaction> transactions)
            {
            Context.Transactions.AddRange(transactions);
            var result = Context.SaveChanges();
            return result > 0;
            }
        }
    }

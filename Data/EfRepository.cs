﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class EfRepository<T> where T : Entity
    {
        protected readonly Context Context;
        protected IQueryable<T> Query;

        public EfRepository(Context context)
        {
            Context = context;
        }

        public virtual T GetById(int id)
        {
            return Query.FirstOrDefault(x => x.Id == id);
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await Query.FirstOrDefaultAsync(x => x.Id == id);
        }

        public IList<T> ListAll()
        {
            return Query.ToList();
        }

        public async Task<IList<T>> ListAllAsync()
        {
            return await Query.ToListAsync();
        }

        public virtual T Add(T entity)
        {
            Context.Set<T>().Add(entity);
            Context.SaveChanges();

            return entity;
        }

        public void AddRange(IEnumerable<T> entities)
        {
            Context.Set<T>().AddRange(entities);
            Context.SaveChanges();
        }

        public virtual async Task<T> AddAsync(T entity)
        {
            Context.Set<T>().Add(entity);
            await Context.SaveChangesAsync();

            return entity;
        }

        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            await Context.Set<T>().AddRangeAsync(entities);
            await Context.SaveChangesAsync();
        }

        public virtual void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public virtual async Task UpdateAsync(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            await Context.SaveChangesAsync();
        }

        public void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
            Context.SaveChanges();
        }

        public async Task DeleteAsync(T entity)
        {
            Context.Set<T>().Remove(entity);
            await Context.SaveChangesAsync();
        }
    }
}
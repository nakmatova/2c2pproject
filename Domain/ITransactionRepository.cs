﻿using System;
using System.Collections.Generic;
using Domain.Entities;
using Domain.Models;

namespace Domain
{
    public interface ITransactionRepository
    {
        List<TransactionViewModel> GetTransactionsByCurrency(string currency);
        List<TransactionViewModel> GetTransactionsByDateRange(DateTime start, DateTime end);
        List<TransactionViewModel> GetTransactionsByStatus(short status);
        bool AddTransactionsList(List<Transaction> transactions);
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Domain
    {
        public static class EnumExtensions
        {
            public static string GetDisplayName(this Enum enumVal)
            {
                if (enumVal == null)
                    return "";

                var type = enumVal.GetType();
                var memInfo = type.GetMember(enumVal.ToString());
                var attributes = memInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false);
                if (attributes.Length != 0)
                    return ((DisplayAttribute)attributes[0]).Name;

                var aisAttributes = memInfo[0].GetCustomAttributes(typeof(EnumMemberAttribute), false);
                if (aisAttributes.Length != 0)
                    return ((EnumMemberAttribute)aisAttributes[0]).Value;

                return enumVal.ToString();
            }
        }
    }

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Domain.Entities;
using Domain.Models;

namespace Domain
{
    public class XmlParser : IParser<Transaction>
    {
        public string _logs = "";

        public ResponseModel<List<Transaction>> Parse(Stream stream)
        {
            var transactions = new List<Transaction>();
            var streamReader = new StreamReader(stream);
            using (var stringReader = new StringReader(streamReader.ReadToEnd()))
            using (var reader = new XmlTextReader(stringReader))
            {
                var transaction = new Transaction();
                var i = 1;
                while (reader.Read())
                {
                    if (!reader.IsStartElement()) continue;
                    switch (reader.Name)
                    {
                        case "Transaction":
                            var id = reader.GetAttribute("id");
                            if (string.IsNullOrWhiteSpace(id)) AddLog($" id value of row #{i} is empty");
                            else transaction.TransactionId = id;
                            i++;
                            break;
                        case "TransactionDate":
                            var date = reader.ReadString();
                            if (string.IsNullOrWhiteSpace(date))
                                AddLog($" TransactionDate value of row #{i} is empty");
                            
                            else transaction.TransactionDate = DateTime.Parse(date);
                            break;
                        case "Amount":
                            var amount = reader.ReadString();
                            if (string.IsNullOrWhiteSpace(amount))  AddLog($" Amount value of row #{i} is empty");
                            else transaction.Amount = Convert.ToDecimal(amount, CultureInfo.InvariantCulture);
                            break;
                        case "CurrencyCode":
                            var currency = reader.ReadString();
                            if (string.IsNullOrWhiteSpace(currency)) AddLog($" CurrencyCode value of row #{i} is empty");
                            transaction.CurrencyCode = currency;
                            break;
                        case "Status":
                            var statusValue = reader.ReadString();
                            if (string.IsNullOrWhiteSpace(statusValue)) AddLog($" Status value of row #{i} is empty");
                            else
                            {
                                var status = (TransactionModelXML.Status) Enum.Parse(typeof(TransactionModelXML.Status),
                                    statusValue);

                                transaction.Status = (short) status;
                            }

                            transactions.Add(transaction);
                            break;
                    }

                    
                }
            }

            if (!string.IsNullOrWhiteSpace(_logs))
                return new ResponseModel<List<Transaction>>
                {
                    IsSuccess = false,
                    Response = null,
                    Error = new ApiError(_logs)
                };

            return new ResponseModel<List<Transaction>>
            {
                IsSuccess = true,
                Response = transactions
            };
        }

        private void AddLog(string value)
        {
            _logs += value;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Models;

namespace Domain
    {
        public interface IParser<T>
        {
            ResponseModel<List<T>> Parse(Stream stream);
        }
    }

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Domain.Entities;
using Domain.Models;

namespace Domain
{
    public class CsvParser : IParser<Transaction>
    {
        public string _logs = "";
        public  ResponseModel<List<Transaction>> Parse(Stream stream)
        {
            var transactions = new List<Transaction>();
            _logs = "";
            using (TextReader reader = new StreamReader(stream))
            {
                var line = reader.ReadLine();
                var i = 1;
                while (line != null)
                {
                    var columns = line.Split(';');
                    var transaction = new Transaction();
                    var id = columns[0];
                    if (string.IsNullOrWhiteSpace(id)) AddLog($" TransactionId value of row #{i} is empty");
                    else transaction.TransactionId = id;

                    var amount = columns[1];
                    if (string.IsNullOrWhiteSpace(amount)) AddLog($" Amount value of row #{i} is empty");
                    else transaction.Amount =  Convert.ToDecimal(amount, CultureInfo.InvariantCulture);

                    var currencyCode = columns[2];
                    if (string.IsNullOrWhiteSpace(currencyCode)) AddLog($" CurrencyCode value of row #{i} is empty");
                    else transaction.CurrencyCode = currencyCode;

                    var trancationDate = columns[3];
                    if (string.IsNullOrWhiteSpace(trancationDate)) AddLog($" TrancationDate value of row #{i} is empty");
                    else transaction.TransactionDate = DateTime.Parse(trancationDate);

                    var status = columns[4];
                    if (string.IsNullOrWhiteSpace(status))
                        AddLog($" Status valueof row #{i} is empty");
                    else
                        transaction.Status = (short)(TransactionModelCSV.Status)Enum.Parse(typeof(TransactionModelCSV.Status),
                       columns[4]);

                    transactions.Add(transaction);
                    line = reader.ReadLine();
                    i++;
                }
            }

            if (!string.IsNullOrWhiteSpace(_logs)) return new ResponseModel<List<Transaction>>
            {
                IsSuccess = false,
                Response = null,
                Error = new ApiError(_logs)
            };

            return new ResponseModel<List<Transaction>>
            {
                IsSuccess = true,
                Response = transactions
            };
        }

        private void AddLog(string value)
        {
            _logs += value;
        }
    }
}
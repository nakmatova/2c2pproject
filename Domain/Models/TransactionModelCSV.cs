﻿using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System;

namespace Domain.Models
{
    public class TransactionModelCSV: TransactionModel
    {
        public string TransactionIdentificator { get; set; }
        public Status TransactionStatus { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public enum Status
        {
            Undefined = 0,
            Approved = 1,
            Failed = 2,
            Finished = 3
        }
    }
}

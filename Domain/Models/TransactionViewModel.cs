﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class TransactionViewModel
    {
        public string id { get; set; }
        public string payment { get; set; }
        public string Status { get; set; }
    }
}
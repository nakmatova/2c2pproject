﻿using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System;

namespace Domain.Models
{
    public class TransactionModelXML : TransactionModel
    {
        public string TransactionId { get; set; }
        public Status TransactionStatus { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public enum Status
        {
            Undefined = 0,
            Approved= 1,
            Rejected = 2,
            Done = 3
        }
    }
}

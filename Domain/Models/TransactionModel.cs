﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public abstract class TransactionModel
    {
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime TrancationDate { get; set; }
    }

    public enum Status
    {
        Undefined = 0,
        A = 1,
        R = 2,
        D = 3
    }
}
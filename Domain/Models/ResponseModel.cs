﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
    {
        public class ResponseModel<T>
        {
            public bool IsSuccess { get; set; }

            public T Response { get; set; }

            public ApiError Error { get; set; }

            public static ResponseModel<T> Fail(ApiError response)
            {
                var request = new ResponseModel<T>
                {
                    IsSuccess = false,
                    Error = response,
                    Response = default(T)
                };
                return request;
            }

            public static ResponseModel<T> Success(T response)
            {
                var request = new ResponseModel<T>
                {
                    IsSuccess = true,
                    Response = response
                };
                return request;
            }
        }
    }

﻿using System;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Api.Controllers
    {
        [Route("api/[controller]/[action]")]
        [ApiController]
        public class TransactionsController : ControllerBase
        {
        private readonly ITransactionRepository _repository;

        public TransactionsController(ITransactionRepository repository)
            {
            _repository = repository;
            }

        // GET api/transactions/GetByCurrency/USD
        [HttpGet("{currency}")]
        public ActionResult<string> GetByCurrency(string currency)
            {
            var list = _repository.GetTransactionsByCurrency(currency);

            return JsonConvert.SerializeObject(list);
            }

        // GET api/transactions/GetByStatus/1
        [HttpGet("{status}")]
        public ActionResult<string> GetByStatus(short status)
            {
            var list = _repository.GetTransactionsByStatus(status);

            return JsonConvert.SerializeObject(list);
            }

        // GET api/transactions/GetByDate/2019-12-25/2019-12-31
        [HttpGet("{start}/{end}")]
        public ActionResult<string> GetByDate(DateTime start, DateTime end)
            {
            var list = _repository.GetTransactionsByDateRange(start, end);

            return JsonConvert.SerializeObject(list);
            }
        }
    }

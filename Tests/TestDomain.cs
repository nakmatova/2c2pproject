﻿using System;
using System.IO;
using System.Text;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class TestDomain
    {
        [TestMethod]
        public void CanParseCsv()
        {
            var data = @"Invoice0000001;1000.00;USD;20.02.2019 12:33;Approved
Invoice0000002;300.00;USD;21.02.2019 2:04;Failed";
            var byteArray = Encoding.ASCII.GetBytes(data);
            var stream = new MemoryStream(byteArray);

            var parser = new CsvParser();
            var response = parser.Parse(stream);

            Assert.IsTrue(response.IsSuccess);
            Assert.AreEqual(2, response.Response.Count);
        }

        [TestMethod]
        public void CanParseXml()
        {
            var data = @"<Transactions>
                <Transaction id=""Inv00001"">
                <TransactionDate>2019-01-23T13:45:10</TransactionDate>
                <PaymentDetails>
                <Amount>200.00</Amount>
                <CurrencyCode>USD</CurrencyCode>
                </PaymentDetails>
                <Status>Done</Status>
                </Transaction>
                <Transaction id=""Inv00002"">
                <TransactionDate>2019-01-24T16:09:15</TransactionDate>
                <PaymentDetails>
                <Amount>10000.00</Amount>
                <CurrencyCode>EUR</CurrencyCode>
                </PaymentDetails>
                <Status>Rejected</Status>
                </Transaction>
                </Transactions>";
            var byteArray = Encoding.ASCII.GetBytes( data );
            var stream = new MemoryStream( byteArray );

            var parser = new XmlParser();
            var response = parser.Parse(stream);

            Assert.IsTrue(response.IsSuccess);
            Assert.AreEqual(2,response.Response.Count);
        }
    }
}
CREATE DATABASE "2c2p"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- Table: public.transaction

-- DROP TABLE public.transaction;

CREATE TABLE public.transaction
(
    id serial,
    identificator character varying(50) COLLATE pg_catalog."default" NOT NULL,
    amount numeric NOT NULL,
    currency_code character varying(3) COLLATE pg_catalog."default" NOT NULL,
    date timestamp without time zone NOT NULL,
    status smallint NOT NULL,
    CONSTRAINT transaction_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.transaction
    OWNER to postgres;
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Domain;
using Domain.Entities;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITransactionRepository _repository;

        public HomeController(ITransactionRepository repository)
        {
            _repository = repository;
        }

        public IActionResult Index(List<string> logs, HttpStatusCode statusCode)
        {
            ViewBag.Logs = logs;
            ViewData["Status"] = statusCode;
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        [HttpPost]
        [RequestSizeLimit(1000000)]
        public async Task<IActionResult> ImportFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return RedirectToIndex(new {Logs = "File is empty", statusCode = HttpStatusCode.ExpectationFailed});

            if (!IsCsv(file) && !IsXml(file))
                return RedirectToIndex(new {logs = "Unknown format", statusCode = HttpStatusCode.ExpectationFailed});

            var response = new ResponseModel<List<Transaction>>();

            if (IsCsv(file))
            {
                var csvParser = new CsvParser();
                response = csvParser.Parse(file.OpenReadStream());
            }

            if (IsXml(file))
            {
                var xmlParser = new XmlParser();
                response = xmlParser.Parse(file.OpenReadStream());
            }

            if (response.IsSuccess)
            {
                var result = _repository.AddTransactionsList(response.Response);
                return result
                    ? RedirectToIndex(new {logs = "", statusCode = HttpStatusCode.OK})
                    : RedirectToIndex(new
                        {logs = "Error while save transactions", statusCode = HttpStatusCode.InternalServerError});
            }

            return RedirectToIndex(new {logs = response.Error.Message, statusCode = HttpStatusCode.BadRequest});
        }

        private RedirectToActionResult RedirectToIndex(object response)
        {
            return RedirectToAction("Index", response);
        }

        private static bool IsXml(IFormFile file)
        {
            return file.FileName.ToLower().EndsWith(".xml");
        }

        private static bool IsCsv(IFormFile file)
        {
            return file.FileName.ToLower().EndsWith(".csv");
        }
    }
}